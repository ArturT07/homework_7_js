function filterBy(arr, dataType) {
    return arr.filter((item) => typeof item !== dataType);
}
const originalArray = ['hello', 'world', 23, '23', null];
const filteredArray = filterBy(originalArray, 'string');
console.log(filteredArray);
